<?php
namespace App\Tests\Entity;

use App\Entity\Item;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class ItemTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): Item
    {
        return (new Item())
            ->setName("Cours")
            ->setContent("Description de test")
            ->setCreatedAt(new \DateTime());
    }

    public function assertHasErrors(Item $item, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($item);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(", ", $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 1);
    }

    public function testInvalidBlankNameEntity()
    {
        $this->assertHasErrors($this->getEntity()->setName(""), 2);
    }

    public function testInvalidLengthContentEntity()
    {
        $this->assertHasErrors($this->getEntity()->setContent("Ce compteur de caractères en ligne et gratuit est un logiciel qui permet de compter le nombre de caractères d'un texte en quasi temps réel ainsi que le nombre de lettres. Par défaut, ce compteur de lettres compte tous les caractères : espace compris. Il est possible parfois de compter les caractères mais sans les espaces, sans les tabulations ou sans les sauts de ligne.
        Ce compteur de caractères en ligne et gratuit est un logiciel qui permet de compter le nombre de caractères d'un texte en quasi temps réel ainsi que le nombre de lettres. Par défaut, ce compteur de lettres compte tous les caractères : espace compris. Il est possible parfois de compter les caractères mais sans les espaces, sans les tabulations ou sans les sauts de ligne.
        Ce compteur de caractères en ligne et gratuit est un logiciel qui permet de compter le nombre de caractères d'un texte en quasi temps réel ainsi que le nombre de lettres. Par défaut, ce compteur de lettres compte tous les caractères : espace compris. Il est possible parfois de compter les caractères mais sans les espaces, sans les tabulations ou sans les sauts de ligne.
        Ce compteur de caractères en ligne et gratuit est un logiciel qui permet de compter le nombre de caractères d'un texte en quasi temps réel ainsi que le nombre de lettres. "), 2);
    }

    public function testInvalidUsedName ()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/fixtures/item.yaml']);
        $this->assertHasErrors($this->getEntity()->setName('Cours1'), 1);
    }



}
