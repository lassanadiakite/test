<?php
namespace App\Tests\Entity;

use App\Entity\TodoList;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class TodoListTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): TodoList
    {
        return (new TodoList())
            ->setName("Cours");
    }
    public function assertHasErrors(TodoList $todolist, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($todolist);
        $messages = [];
        $this->loadFixtureFiles([dirname(__DIR__) . '/fixtures/todo_list.yaml']);
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(", ", $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 1);
    }

    public function testInvalidBlankNameEntity()
    {
        $this->assertHasErrors($this->getEntity()->setName(""), 2);
    }

    

    public function testInvalidtodo ()
    {
        $this->loadFixtureFiles([dirname(__DIR__) . '/fixtures/todo_list.yaml']);
        $this->assertHasErrors($this->getEntity(), 0);
    }



}
