<?php
namespace App\Tests\Entity;

use App\Entity\User;
use Liip\TestFixturesBundle\Test\FixturesTrait;
use Symfony\Bundle\FrameworkBundle\Test\KernelTestCase;
use Symfony\Component\Validator\ConstraintViolation;

class UserTest extends KernelTestCase
{

    use FixturesTrait;

    public function getEntity(): User
    {
        return (new User())
            ->setFirstname("Lassana")
            ->setLastname("DIAKITE")
            ->setEmail("lassana@gmail.com")
            ->setPassword("password")
            ->setAge(new \DateTime());
    }

    public function assertHasErrors(User $user, int $number = 0)
    {
        self::bootKernel();
        $errors = self::$container->get('validator')->validate($user);
        $messages = [];
        /** @var ConstraintViolation $error */
        foreach($errors as $error) {
            $messages[] = $error->getPropertyPath() . ' => ' . $error->getMessage();
        }
        $this->assertCount($number, $errors, implode(", ", $messages));
    }

    public function testValidEntity()
    {
        $this->assertHasErrors($this->getEntity(), 0);
    }

    public function testInvalidBlankFirstnameEntity()
    {
        $this->assertHasErrors($this->getEntity()->setFirstname(""), 1);
    }

    public function testInvalidBlanklastnameEntity()
    {
        $this->assertHasErrors($this->getEntity()->setLastname(""), 1);
    }

    public function testInvalidLengthMinPasswordEntity()
    {
        $this->assertHasErrors($this->getEntity()->setPassword("0123"), 1);
    }

    public function testInvalidLengthMaxPasswordEntity()
    {
        $this->assertHasErrors($this->getEntity()->setPassword("012345678901234567890123456789012345678901234567890123456789"), 1);
    }


    

    // public function testInvalidUsedName ()
    // {
    //     $this->loadFixtureFiles([dirname(__DIR__) . '/fixtures/item.yaml']);
    //     $this->assertHasErrors($this->getEntity()->setName('Cours1'), 2);
    // }



}
